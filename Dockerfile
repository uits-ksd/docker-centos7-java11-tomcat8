# Tomcat 8
ARG  DOCKER_REGISTRY
ARG  BASE_IMAGE_TAG_DATE
FROM $DOCKER_REGISTRY/kuali/tomcat8:java11-ua-release-$BASE_IMAGE_TAG_DATE
LABEL maintainer="U of A Kuali DevOps <katt-support@list.arizona.edu>"

# Set environment variables
ENV CATALINA_HOME /opt/tomcat

# Install updates
RUN yum update -y && \
    rm -rf /var/lib/apt/lists/*

# Manually install Tomcat from included download
COPY bin .
RUN tar -xvf apache-tomcat-8.5.93.tar.gz && \
    rm apache-tomcat-8.5.93.tar.gz && \
    mv apache-tomcat* ${CATALINA_HOME}

RUN chmod +x $CATALINA_HOME/bin/*sh

# Remove suggested files and folders (from https://wiki.owasp.org/index.php/Securing_tomcat)
RUN rm -rf $CATALINA_HOME/webapps/ROOT/*
RUN rm -rf $CATALINA_HOME/webapps/docs/
RUN rm -rf $CATALINA_HOME/webapps/examples/
RUN rm -rf $CATALINA_HOME/webapps/host-manager/
RUN rm -rf $CATALINA_HOME/webapps/manager/

# Add basic error.jsp to ROOT folder for FIN-2165
COPY templates/error.jsp $CATALINA_HOME/webapps/ROOT/

# Create links that KFS image expects
RUN ln -s ${CATALINA_HOME} /var/lib/tomcat
  
EXPOSE 8080