# University of Arizona Java 11 and Tomcat 8 Docker Image

---

This repository is for the Kuali team's Tomcat 8 image based on their CentOS 7 + Java 11 Docker image. It was originally based on the CentOS 8 + Java 11 image, but CentOS 8 reached EOL earlier than expected.

### Description
This project defines an image that will be the base image for the Kuali team's KFS and Rice Docker images.

### Requirements
This is based on a **java11** tagged image from the _397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/tomcat8_ AWS ECR repository.

The included version of Apache Tomcat was downloaded from https://archive.apache.org/dist/tomcat/tomcat-8/v8.5.93/bin/ to reduce external dependencies at build time.

### Building
#### Locally
If you are testing changes and building the image locally, follow these steps:

##### Without AWS Credentials
1. Make sure you have a `kuali/tomcat8:java11-ua-release-$BASE_IMAGE_TAG_DATE` image in your Docker repository. (This may require a local build of the *java11* base image).
2. Temporarily change the Dockerfile to define the base image as `FROM kuali/tomcat8:java11-ua-release-$BASE_IMAGE_TAG_DATE`.
3. Temporarily add this to the end of the Dockerfile: `ENTRYPOINT /usr/local/bin/local-testing.sh`.
4. Run this command to build a *java11tomcat8* Docker image, replacing $BASE_IMAGE_TAG_DATE with the date referenced in step 1: `docker build --build-arg BASE_IMAGE_TAG_DATE=$BASE_IMAGE_TAG_DATE -t kuali/tomcat8:java11tomcat8-ua-release-$BASE_IMAGE_TAG_DATE .`
5. Use a command like this after you've successfully created a local test image, substituting the BASE_IMAGE_TAG_DATE you used in the build command: docker run -d --name=java11tomcat8 --privileged=true kuali/tomcat8:java11tomcat8-ua-release-$BASE_IMAGE_TAG_DATE
6. Make sure to remove the base image and ENTRYPOINT instruction added to the Dockerfile for testing before you commit your changes.

##### With AWS Credentials
1. Follow instructions on Confluence to get temporary AWS credentials: [Get Temporary AWS credentials for an account](https://confluence.arizona.edu/display/KAST/Get+Temporary+AWS+credentials+for+an+account)
2. Build the image with the same command used in Jenkins. For example: `docker build --build-arg DOCKER_REGISTRY=397167497055.dkr.ecr.us-west-2.amazonaws.com --build-arg BASE_IMAGE_TAG_DATE=2020-05-26 -t kuali/tomcat8 .`

This requires a local install of the AWS CLI: https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html.

_This AWS Credentials method is untested at this current time._

#### Jenkins
The build command we use is: `docker build --build-arg DOCKER_REGISTRY=${DOCKER_REGISTRY} --build-arg BASE_IMAGE_TAG_DATE=${BASE_IMAGE_TAG_DATE} -t ${DOCKER_REPOSITORY_NAME}:${TAG} .`

* `$DOCKER_REGISTRY` is the location of the Docker image repository in AWS. The value will be a variable in our Jenkins job and defined as `397167497055.dkr.ecr.us-west-2.amazonaws.com`.
* `$BASE_IMAGE_TAG_DATE` corresponds to the creation date in a tag of the *java11* Docker image.
* `$DOCKER_REPOSITORY_NAME` is the name of the AWS ECR repository, which is _kuali/tomcat8_.
* `$TAG` is the image tag we construct, which is _java11tomcat8-ua-release-_ plus the current date in YYYY-MM-DD format.

We then tag with the build date and push the image to AWS with these commands: 
```
docker tag ${DOCKER_REPOSITORY_NAME}:${TAG} ${DOCKER_REPOSITORY_URI}:${TAG}
docker push ${DOCKER_REPOSITORY_URI}:${TAG}
```

Example of a resulting tag: _397167497055.dkr.ecr.us-west-2.amazonaws.com/kuali/tomcat8:java11tomcat8-ua-release-2020-05-26_.

Jenkins link: https://kfs-jenkins.ua-uits-kuali-nonprod.arizona.edu/job/Development/view/Docker%20Baseline/

### Maintenance Notes
If a newer *java11* image has been created, a newer version of this *java11tomcat8* image will need to be created. The date is controlled by the value for the `$BASE_IMAGE_TAG_DATE`.

The Dockerfiles for KFS and Rice will need to be updated to use this new version of the java11tomcat8 image based on the creation date.